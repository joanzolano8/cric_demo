import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { TablaPersonaComponent } from './pages/tabla-persona/tabla-persona.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'cric-app';
  constructor(private dialog: MatDialog) {
  }
  ngOnInit(): void {
  }
  
  prueba(){
    setTimeout(() => {
      const id = localStorage.getItem("id_capa");
      console.log(id);
      if (id != null) {
        const modal = this.dialog.open(TablaPersonaComponent,{
          maxWidth: '100%',
          maxHeight: '100%',
          height: '90%',
          width: '90%',
          data: { id: id}
        });
        modal.afterClosed().subscribe( () =>{
          localStorage.removeItem("id_capa");
        });
      }
    }, 1000)
  }
}
