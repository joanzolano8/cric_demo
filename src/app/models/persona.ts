export class Persona {
    id: string;
    nombres: string;
    apellidos: string;
    identificacion: string;
    idResguardo: string;
    municipio: string;
    resguardo: string;
    tipoSaber: string;
    unidadCuidado: string;
    vereda: string;
    zona: string;
}