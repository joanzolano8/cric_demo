import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CrudService } from '../../services/crud.service';
import { Persona } from '../../models/persona';

@Component({
  selector: 'app-tabla-persona',
  templateUrl: './tabla-persona.component.html',
  styleUrls: ['./tabla-persona.component.css']
})
export class TablaPersonaComponent implements OnInit {

  id: number;
  listaPersona: Persona[] = [];
  listadoTabla: Persona[] = [];
  pagina: number = 1;

  constructor(public dialogRef: MatDialogRef<TablaPersonaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private crud: CrudService) {
    this.id = data.id;
  }

  ngOnInit() {
    this.crud.listaPersonas(this.id).subscribe((resp: Persona[]) =>{
      this.listaPersona = resp;
      this.paginacion(null);
    });
  }
  paginacion(operacion: boolean){
    const validacionPaginacion = this.pagina < (this.listaPersona.length / 5) && this.pagina >= 1 ;
    if (operacion && this.pagina < (this.listaPersona.length / 5)) {
      this.pagina++;
    } else if (!operacion && this.pagina > 1) {
      this.pagina--;
    }
    this.listadoTabla = this.listaPersona.filter((persona, index) => index < (this.pagina * 5) && index >= ((this.pagina - 1) * 5));
  }

}
