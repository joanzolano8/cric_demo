import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CrudService {
  
  private url = "http://127.0.0.1:8080/";  
  constructor(private http: HttpClient) { }

  listaPersonas(idResguardo){
    return this.http.get(this.url + "test/resguardo/" + idResguardo);
  }
}
